function rmmLSHADE(Runs,fhd,C,problem_size,funcs,max_nfes,pop_size,optimum)

Rand_Seeds=load('input_data/Rand_Seeds.txt');

Alg_Name=[ 'rmmLSHADE_(' num2str(C(1)) num2str(C(2)) num2str(C(3)) ')'];

F =0.50*ones(pop_size,problem_size);
Cr=0.90*ones(pop_size,problem_size);

lu = [-100 * ones(1, problem_size); 100 * ones(1, problem_size)];

fprintf('Running %s algorithm on D= %d\n',Alg_Name, problem_size)

for n=1:max_nfes/100
    RecordFEsFactor(n) = n*100;
end

progress = numel(RecordFEsFactor);
val_2_reach = 10^(-8);

for func_no = funcs
    fprintf('\n-------------------------------------------------------\n')
    fprintf('Function = %d, Dimension size = %d\n', func_no, problem_size)
    allerrorvals = zeros(progress, Runs);
%     you can use parfor if you have MATLAB Parallel Computing Toolbox
    parfor run_id = 1 : Runs
        rand_ind = mod(problem_size*func_no*Runs+run_id-Runs,length(Rand_Seeds));
        run_seed=Rand_Seeds(max(1, rand_ind));
        rng(run_seed,'twister');
        Run_RecordFEsFactor=RecordFEsFactor;
        run_funcvals = [];
        
        %%  parameter settings for L-SHADE
        p_best_rate = 0.11;
        arc_rate = 1.4;
        memory_size = 5;
        pop_size = 18 * problem_size;
        
        max_pop_size = pop_size;
        min_pop_size = 4.0;

        % Meta-model parameters
        pop_size_multiplier = 5;
        df_quad = 2*problem_size+1;
        df_full = 2*problem_size + problem_size*(problem_size-1)/2 + 1;

        % Filter Parameters
        p       = df_full;                % filter order
        lambda  = 1.00;              % forgetting factor
        laminv  = 1/lambda;
        delta   = 1.0;              % initialization parameter
        
        
        %% Initialize the main population
        popold = repmat(lu(1, :), pop_size, 1) + rand(pop_size, problem_size) .* (repmat(lu(2, :) - lu(1, :), pop_size, 1));
        pop = popold; % the old population becomes the current population
        
        fitness = feval(fhd, pop', func_no, C);
        fitness = fitness';
        
        nfes = 0;
        bsf_fit_var = 1e+30;
        bsf_solution = zeros(1, problem_size);
        
        %%%% CEC evaluation
        for i = 1 : pop_size
            nfes = nfes + 1;            
            if fitness(i) < bsf_fit_var
                bsf_fit_var = fitness(i);
                bsf_solution = pop(i, :);
            end
            if nfes > max_nfes
                break;
            end
            if(nfes>=Run_RecordFEsFactor(1))
                run_funcvals = [run_funcvals; bsf_fit_var];
                Run_RecordFEsFactor(1)=[];
            end
        end
        %%%% CEC evaluation

        %% Initial model           
        X = [NaN(pop_size, df_full-1), ones(pop_size,1)];
        X(:, 1:df_quad-1) = [pop .* pop, pop];
        %X(:, 1:10) = [pop .* pop];
        col_idx = df_quad;
        for d1 = 1:problem_size
            for d2 = (d1+1):problem_size
                X(:, col_idx) = pop(:,d1) .* pop(:,d2);
                col_idx = col_idx + 1;
            end
        end                 
        b = (X'*X)\X'*fitness;

        % Filter Initialization
        %w = b;       % filter coefficients
        P = delta*eye(p);     % inverse correlation matrix
        %e = x*0;              % error signal        
        %%       
        memory_sf = 0.5 .* ones(memory_size, 1);
        memory_cr = 0.5 .* ones(memory_size, 1);
        memory_pos = 1;

        archive.NP = round(arc_rate * pop_size); % the maximum size of the archive
        archive.pop = zeros(0, problem_size); % the solutions stored in te archive
        archive.funvalues = zeros(0, 1); % the function value of the archived solutions

        %% main loop
        while nfes < max_nfes
            pop = popold; % the old population becomes the current population
            pop_ext = repmat(pop, pop_size_multiplier, 1);
            pop_size_ext = pop_size * pop_size_multiplier;
            
            [temp_fit, sorted_index] = sort(fitness, 'ascend');
            
            mem_rand_index = ceil(memory_size * rand(pop_size_ext, 1));
            mu_sf = memory_sf(mem_rand_index);
            mu_cr = memory_cr(mem_rand_index);
            
            %% for generating crossover rate
            cr = normrnd(mu_cr, 0.1);
            term_pos = find(mu_cr == -1);
            cr(term_pos) = 0;
            cr = min(cr, 1);
            cr = max(cr, 0);
            
            %% for generating scaling factor
            sf = mu_sf + 0.1 * tan(pi * (rand(pop_size_ext, 1) - 0.5));
            pos = find(sf <= 0);
            
            while ~ isempty(pos)
                sf(pos) = mu_sf(pos) + 0.1 * tan(pi * (rand(length(pos), 1) - 0.5));
                pos = find(sf <= 0);
            end
            
            sf = min(sf, 1);
            
            r0 = [1 : pop_size];
            popAll = [pop; archive.pop];
            [r1, r2] = gnR1R2(pop_size, size(popAll, 1), repmat(r0, 1, pop_size_multiplier));
            
            pNP = max(round(p_best_rate * pop_size), 2); %% choose at least two best solutions
            randindex = ceil(rand(1, pop_size_ext) .* pNP); %% select from [1, 2, 3, ..., pNP]
            randindex = max(1, randindex); %% to avoid the problem that rand = 0 and thus ceil(rand) = 0
            pbest = pop(sorted_index(randindex), :); %% randomly choose one of the top 100p% solutions
            
            vi = pop_ext + sf(:, ones(1, problem_size)) .* (pbest - pop_ext + pop(r1, :) - popAll(r2, :));
            vi = boundConstraint(vi, pop_ext, lu);
            
            mask = rand(pop_size_ext, problem_size) > cr(:, ones(1, problem_size)); % mask is used to indicate which elements of ui comes from the parent
            rows = (1 : pop_size_ext)'; 
            cols = floor(rand(pop_size_ext, 1) * problem_size)+1; % choose one position where the element of ui doesn't come from the parent
            jrand = sub2ind([pop_size_ext problem_size], rows, cols); 
            mask(jrand) = false;
            ui = vi; 
            ui(mask) = pop_ext(mask);

            %% Meta-model best candidates
            % Model prediction
            x = [NaN(pop_size_ext, df_full-1), ones(pop_size_ext, 1)];
            x(:,1:df_quad-1) = [ui .* ui, ui];
            %x(:,1:10) = [ui .* ui];
            col_idx = df_quad;
            for d1 = 1:problem_size
                for d2 = (d1+1):problem_size
                    x(:,col_idx) = ui(:,d1) .* ui(:,d2);
                    col_idx = col_idx + 1;
                end
            end       
            
            %%
            children_fitness = fitness;
            indeces_best = NaN(pop_size,1);
            for i = 1 : pop_size
                % Surr-evaluate each element
                y_surr = (b'*x')';
                % Get the best one
                [~, indeces_best_all] = sort(y_surr);
                % Add to indeces list
                indeces_best_diff = setdiff(indeces_best_all, indeces_best, 'stable');
                indeces_best(i) = indeces_best_diff(1);
                % Evaluate new element
                children_fitness(i) = feval(fhd, ui(indeces_best(i),:)', func_no ,C);
                % RLS
                % Error signal equation
                e = children_fitness(i) - b'*x(indeces_best(i),:)';  
                % Parameters for efficiency
                Pi = P*x(indeces_best(i),:)';                   
                % Filter gain vector update
                k = (Pi)/(lambda+x(indeces_best(i),:)*Pi);
                % Inverse correlation matrix update
                P = (P - k*x(indeces_best(i),:)*P)*laminv;
                % Filter coefficients adaption
                b = b + k*e;
            end
            %%
                       
            % Shrink trial vectors, sf parameters and cf parameters 
            ui = ui(indeces_best, :);          
            sf = sf(indeces_best);
            cr = cr(indeces_best);
            %x = x(indeces_best, :);
            %y_surr = y_surr(indeces_best);
           
            %%%% CEC evaluation
            for i = 1 : pop_size
                nfes = nfes + 1;    
                if children_fitness(i) < bsf_fit_var
                    bsf_fit_var = children_fitness(i);
                end 
                if nfes > max_nfes
                    break;
                end
                if(nfes>=Run_RecordFEsFactor(1))
                    run_funcvals = [run_funcvals; bsf_fit_var];
                    Run_RecordFEsFactor(1)=[];
                end
            end
            %%%% CEC evaluation
            
            dif = abs(fitness - children_fitness);
            
            %% I == 1: the offspring is better; I == 0: the parent is better
            I = (fitness > children_fitness);

            goodCR = cr(I == 1);
            goodF = sf(I == 1);
            dif_val = dif(I == 1);
            
            indeces_origin = mod(indeces_best, pop_size);
            indeces_origin(indeces_origin == 0) = pop_size;
            poporigin = popold(indeces_origin, :);
            fitnessorigin = fitness(indeces_origin, :);
            archive = updateArchive(archive, poporigin(I == 1, :), fitnessorigin(I == 1));            
            [fitness, I] = min([fitness, children_fitness], [], 2);
            
            popold = pop;
            popold(I == 2, :) = ui(I == 2, :);
            
            num_success_params = numel(goodCR);
            
            if num_success_params > 0
                sum_dif = sum(dif_val);
                dif_val = dif_val / sum_dif;  
                
                %% for updating the memory of scaling factor 
                memory_sf(memory_pos) = (dif_val' * (goodF .^ 2)) / (dif_val' * goodF);
                
                %% for updating the memory of crossover rate
                if max(goodCR) == 0 || memory_cr(memory_pos)  == -1
                    memory_cr(memory_pos)  = -1;
                else
                    memory_cr(memory_pos) = (dif_val' * (goodCR .^ 2)) / (dif_val' * goodCR);
                end
                
                memory_pos = memory_pos + 1;
                
                if memory_pos > memory_size
                    memory_pos = 1;
                end
            end
            
            %% for resizing the population size
            plan_pop_size = round((((min_pop_size - max_pop_size) / max_nfes) * nfes) + max_pop_size);
            
            if pop_size > plan_pop_size
                reduction_ind_num = pop_size - plan_pop_size;
                if pop_size - reduction_ind_num <  min_pop_size
                    reduction_ind_num = pop_size - min_pop_size;
                end
                
                pop_size = pop_size - reduction_ind_num;
                
                for r = 1 : reduction_ind_num
                    [valBest, indBest] = sort(fitness, 'ascend');
                    worst_ind = indBest(end);
                    popold(worst_ind,:) = [];
                    pop(worst_ind,:) = [];
                    fitness(worst_ind,:) = [];
                end
                
                archive.NP = round(arc_rate * pop_size);
                
                if size(archive.pop, 1) > archive.NP
                    rndpos = randperm(size(archive.pop, 1));
                    rndpos = rndpos(1 : archive.NP);
                    archive.pop = archive.pop(rndpos, :);               
                end
            end
        end
        
        if(C(1)==1)
            run_funcvals=run_funcvals-optimum(func_no);
        end
        
        run_funcvals(run_funcvals<val_2_reach)=0;
        
        fprintf('%d th run, best-so-far error value = %1.8e\n', run_id , run_funcvals(end))
        allerrorvals(:, run_id) = run_funcvals;
        
    end %% end 1 run
    
    [~, sorted_index] = sort(allerrorvals(end,:), 'ascend');
    allerrorvals = allerrorvals(:, sorted_index);
    
    fprintf('min_funvals:\t%e\n',min(allerrorvals(end,:)));
    fprintf('median_funvals:\t%e\n',median(allerrorvals(end,:)));
    fprintf('mean_funvals:\t%e\n',mean(allerrorvals(end,:)));
    fprintf('max_funvals:\t%e\n',max(allerrorvals(end,:)));
    fprintf('std_funvals:\t%e\n',std(allerrorvals(end,:)));
    
    file_name=sprintf('Results/%s_%s_%s.txt',Alg_Name,int2str(func_no),int2str(problem_size));
    save(file_name, 'allerrorvals', '-ascii');
    
end %% end 1 function run

end
